module vga # ( parameter HDISP = 800, 
               parameter VDISP = 480
             )
            (
    input logic pixel_clk,
    input logic pixel_rst,

    //Interface video
    video_if.master video_ifm,

    //Interface  bus wishbone
    wshb_if.master wshb_ifm  
) ;

    //Definition des parametres locaux 
    localparam HFP = 40 ;
    localparam HPULSE = 48 ;
    localparam HBP = 40 ;
    localparam VFP = 13 ;
    localparam VPULSE = 3 ;
    localparam VBP = 29 ;   

    //Parametres supplementaires
    localparam SUPPRV = VFP + VPULSE + VBP ;
    localparam TOTV = SUPPRV + VDISP ;

    localparam SUPPRH = HFP + HPULSE + HBP ;
    localparam TOTH = SUPPRH + HDISP ;

    //Signal video_ifm.CLK
    assign video_ifm.CLK = pixel_clk ; 

    //Definition de la taille des compteurs 

    logic [$clog2(TOTH) - 1:0] pixel_cmpt ;
    logic [$clog2(TOTV) - 1:0] line_cmpt ;
    


    // Incrementation des compteurs
    always @(posedge pixel_clk or posedge pixel_rst)

        if(pixel_rst)
        begin 
            pixel_cmpt <= 0 ;
            line_cmpt <= 0 ;
        end

        else 
            if (pixel_cmpt == TOTH-1)
            begin
                pixel_cmpt <= 0 ;
                if (line_cmpt == TOTV-1)
                    line_cmpt <= 0 ;
                else    
                    line_cmpt <= line_cmpt + 1'b1 ;
            end
            else    
                pixel_cmpt <= pixel_cmpt + 1'b1 ;

    
    // Generation des signaux HS, VS et BLANK
    always @(posedge pixel_clk or posedge pixel_rst)

        if (pixel_rst)
        begin   
            video_ifm.HS <= 1 ;
            video_ifm.VS <= 1 ;
            video_ifm.BLANK <= 0 ;
        end

        else 
        begin 
            video_ifm.HS <= (pixel_cmpt < HFP || pixel_cmpt >= HFP + HPULSE) ;
            video_ifm.VS <= (line_cmpt < VFP || line_cmpt >= VFP + VPULSE) ;
            video_ifm.BLANK <= (pixel_cmpt >= SUPPRH && line_cmpt >= SUPPRV) ;
        end

    /* // Generation d'une mire 

    //Generation d'un signal de position dans la vraie image
    logic [$clog2(TOTH) - 1:0] real_pix ;
    logic [$clog2(TOTV) - 1:0] real_line ;

    always_comb
        if (pixel_cmpt >= SUPPRH) 
            real_pix = pixel_cmpt - SUPPRH ;
        else    
            real_pix = 0 ;

    always_comb
        if (line_cmpt >= SUPPRV) 
            real_line = line_cmpt - SUPPRV;
        else    
            real_line = 0 ;
 

    // Envoie de la réelle mire
    always @(posedge pixel_clk or posedge pixel_rst)

        if (pixel_rst)
            video_ifm.RGB <= 0 ;
        else 
            if (pixel_cmpt < SUPPRH || line_cmpt < SUPPRV)
                video_ifm.RGB <= 0 ;
            else 
                video_ifm.RGB <= {24{real_pix%16 == 0 || real_line%16 == 0}}; */

    /*
    //Maitre bidon, envoie de constantes (pour les premiers tests)

    assign wshb_ifm.dat_ms = 32'hBABECAFE ;
    assign wshb_ifm.adr = '0 ;
    assign wshb_ifm.cyc = 1'b1 ;
    assign wshb_ifm.sel = 4'b1111 ;
    assign wshb_ifm.stb = 1'b1 ; 
    assign wshb_ifm.we = 1'b1 ;
    assign wshb_ifm.cti = '0 ;
    assign wshb_ifm.bte = '0 ; */

    
    // Lecture en SDRAM 
    
    logic [$clog2(HDISP * VDISP) - 1:0] pix_read ; // le pixel (x,y) correspond au pix_read = (HDISP*y + x),  

    assign wshb_ifm.cti = '0 ;
    assign wshb_ifm.bte = '0 ;
    assign wshb_ifm.sel = 4'b0111 ;
    assign wshb_ifm.we = '0 ;

    always @(posedge wshb_ifm.clk or posedge wshb_ifm.rst)

    if (wshb_ifm.rst)
        pix_read <= 0 ; 
    else 
        if (wshb_ifm.ack)
            if (pix_read == ((HDISP * VDISP) - 1) )
                pix_read <= 0 ;
            else
                pix_read <= pix_read + 1'b1 ;
    // On lit a l'adresse de pix_read, il faut décaler de 4 

    assign wshb_ifm.adr = pix_read *4 ; 

    //assign wshb_ifm.stb = 1 ;

    // Instanciation de la FIFO 

    logic wfull, walmost_full, rempty, read, hb_full; 

    logic [31:0] rdata ;

    async_fifo #(.DATA_WIDTH(32)) FIFO (.rst(wshb_ifm.rst), 
                                        .rclk(pixel_clk), 
                                        .read(read), 
                                        .rdata(rdata), 
                                        .rempty(rempty), 
                                        .wclk(wshb_ifm.clk),
                                        .wdata(wshb_ifm.dat_sm),
                                        .write(wshb_ifm.ack),
                                        .wfull(wfull),
                                        .walmost_full(walmost_full)
                    ) ;

    assign wshb_ifm.stb = ~wfull ; //On n'écrit que si la mémoire tampon n'est pas pleine
    assign wshb_ifm.cyc = ~wfull ;

    assign read = video_ifm.BLANK  & hb_full; // On assigne le signal de lecture en fonction de BLANK

    //Passage de wfull dans le bon domaine d'horloge 
    
    logic wfull1, wfull_pxl ; 

    always @(posedge pixel_clk or posedge pixel_rst)
        if (pixel_rst)
        begin
            wfull1 <= 0 ; 
            wfull_pxl <= 0 ;
        end 

        else 
        begin   
            wfull1 <= wfull ;
            wfull_pxl <= wfull1 ;
        end

    // Generation d'un signal pour savoir si la FIFO a deja ete pleine

    always @(posedge pixel_clk or posedge pixel_rst)
        if (pixel_rst) 
            hb_full <= 0 ;
        else 
            if (wfull_pxl)
                hb_full <= 1 ; 
            else 
                hb_full <= hb_full ; 


    assign video_ifm.RGB = rdata[23:0] ; 

endmodule

            
            




