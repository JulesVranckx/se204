module mire # ( parameter HDISP = 800, 
               parameter VDISP = 480
             )
            (
    //Interface  bus wishbone
    wshb_if.master wshb_ifm  
) ;

    //Compteur pour laisser la main 
    logic [5:0] wait_cmpt ;

    always @(posedge wshb_ifm.clk or posedge wshb_ifm.rst)
        if (wshb_ifm.rst)
            wait_cmpt <= 0 ;
        else    
            wait_cmpt <= (wait_cmpt + 1'b1)%64 ;


    logic [$clog2(HDISP * VDISP)-1 :0]pix_write ;

    // Generation de la position dans l'image

    always @(posedge wshb_ifm.clk or posedge wshb_ifm.rst)

    if (wshb_ifm.rst)
        pix_write <= 0 ; 
    else 
        if (wshb_ifm.ack && wait_cmpt != 63)
            if (pix_write == ((HDISP * VDISP) - 1) )
                pix_write <= 0 ;
            else
                pix_write <= pix_write + 1'b1 ;

    //Signaux ne changeant pas de valeur
    assign wshb_ifm.cti = '0 ;
    assign wshb_ifm.bte = '0 ;
    assign wshb_ifm.sel = 4'b1111 ;
    assign wshb_ifm.we = 1'b1 ;

    assign wshb_ifm.adr = pix_write * 4 ;

    assign wshb_ifm.cyc = (wait_cmpt != 0) ;
    assign wshb_ifm.stb = (wait_cmpt != 0) ;

    assign wshb_ifm.dat_ms = {32{(pix_write%16 == 0)}} ; 
endmodule


    