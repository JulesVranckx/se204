module wshb_intercon # ( parameter HDISP = 800, 
               parameter VDISP = 480
             )
            (
    //Interface  bus wishbone maitre, sortant
    wshb_if.master wshb_ifm,  

    //Interface esclaves
    wshb_if.slave wshb_ifs_mire,
    wshb_if.slave wshb_ifs_vga
) ;

  // Passage du jeton
  enum {mire, vga} jeton ;

  always @(posedge wshb_ifm.clk or posedge wshb_ifm.rst)
    if (wshb_ifm.rst)
      jeton <= mire ;
    
    else 
    begin 
      if (jeton == mire && wshb_ifs_mire.cyc == 0)
        jeton <= vga ;
      else if (jeton == vga && wshb_ifs_vga.cyc == 0)
        jeton <= mire ;
      else
        jeton <= jeton ;
    end

  
  //Signaux en sortie, interface master
  
  logic stb_out, we_out, cti_out, bte_out, cyc_out ;
  logic [3:0] sel_out ;
  logic [31:0] adr_out, dat_out ;

  assign wshb_ifm.cyc = cyc_out ;
  assign wshb_ifm.stb = stb_out ;
  assign wshb_ifm.adr = adr_out ;
  assign wshb_ifm.we = we_out ;
  assign wshb_ifm.dat_ms = dat_out ;
  assign wshb_ifm.sel = sel_out ;
  assign wshb_ifm.cti = cti_out ;
  assign wshb_ifm.bte = bte_out ;

  always_comb
    if (jeton == mire)
    begin
      cyc_out = wshb_ifs_mire.cyc ;
      adr_out = wshb_ifs_mire.adr ;
      we_out = wshb_ifs_mire.we ;
      stb_out = wshb_ifs_mire.stb ;
      dat_out = wshb_ifs_mire.dat_ms ;
      sel_out = wshb_ifs_mire.sel ;
      cti_out = wshb_ifs_mire.cti ;
      bte_out = wshb_ifs_mire.bte ; 
    end
    else 
    begin
      cyc_out = wshb_ifs_vga.cyc ;
      adr_out = wshb_ifs_vga.adr ;
      we_out = wshb_ifs_vga.we ;
      stb_out = wshb_ifs_vga.stb ;
      dat_out = wshb_ifs_vga.dat_ms ;
      sel_out = wshb_ifs_vga.sel ;
      cti_out = wshb_ifs_vga.cti ;
      bte_out = wshb_ifs_vga.bte ;
    end

    // Signaux en sortie de la mire 

    assign wshb_ifs_mire.err = '0 ;
    assign wshb_ifs_mire.rty = '0 ;
    assign wshb_ifs_mire.ack = (wshb_ifm.ack && jeton == mire) ;
    assign wshb_ifs_mire.dat_sm = wshb_ifm.dat_sm ; 

    // Signaux en sortie de vga

    assign wshb_ifs_vga.err = '0 ;
    assign wshb_ifs_vga.rty = '0 ;
    assign wshb_ifs_vga.ack = (wshb_ifm.ack && jeton == vga) ;
    assign wshb_ifs_vga.dat_sm = wshb_ifm.dat_sm ; 

endmodule