`default_nettype none

module Top # (parameter HDISP = 800,
              parameter VDISP = 480)

    (
    // Les signaux externes de la partie FPGA
	input  wire         FPGA_CLK1_50,
	input  wire  [1:0]	KEY,
	output logic [7:0]	LED,
	input  wire	 [3:0]	SW,
    // Les signaux du support matériel son regroupés dans une interface
    hws_if.master       hws_ifm,
    video_if.master video_ifm
);

//====================================
//  Déclarations des signaux internes
//====================================
  wire        sys_rst;   // Le signal de reset du système
  wire        sys_clk;   // L'horloge système a 100Mhz
  wire        pixel_clk; // L'horloge de la video 32 Mhz

//=======================================================
//  La PLL pour la génération des horloges
//=======================================================

sys_pll  sys_pll_inst(
		   .refclk(FPGA_CLK1_50),   // refclk.clk
		   .rst(1'b0),              // pas de reset
		   .outclk_0(pixel_clk),    // horloge pixels a 32 Mhz
		   .outclk_1(sys_clk)       // horloge systeme a 100MHz
);

//=============================
//  Les bus Wishbone internes
//=============================
wshb_if #( .DATA_BYTES(4)) wshb_if_sdram  (sys_clk, sys_rst);
wshb_if #( .DATA_BYTES(4)) wshb_if_stream (sys_clk, sys_rst);

//=============================
//  Le support matériel
//=============================
hw_support hw_support_inst (
    .wshb_ifs (wshb_if_sdram),
    .wshb_ifm (wshb_if_stream),
    .hws_ifm  (hws_ifm),
	.sys_rst  (sys_rst), // output
    .SW_0     ( SW[0] ),
    .KEY      ( KEY )
 );

//=============================
// On neutralise l'interface
// du flux video pour l'instant
// A SUPPRIMER PLUS TARD
//=============================
assign wshb_if_stream.ack = 1'b1;
assign wshb_if_stream.dat_sm = '0 ;
assign wshb_if_stream.err =  1'b0 ;
assign wshb_if_stream.rty =  1'b0 ;

//=============================
// On neutralise l'interface SDRAM
// pour l'instant
// A SUPPRIMER PLUS TARD
//=============================
/*assign wshb_if_sdram.stb  = 1'b0;
assign wshb_if_sdram.cyc  = 1'b0;
assign wshb_if_sdram.we   = 1'b0;
assign wshb_if_sdram.adr  = '0  ;
assign wshb_if_sdram.dat_ms = '0 ;
assign wshb_if_sdram.sel = '0 ;
assign wshb_if_sdram.cti = '0 ;
assign wshb_if_sdram.bte = '0 ; */

//--------------------------
//------- Code Eleves ------
//--------------------------

assign LED[0] = KEY[0] ; 

//Compteur pour le clignotement a 1Hz (LED1 et LED2)
logic [26:0] cpt, cpt2 ; 

//Gestion de la valeur selon si l'on est en simulation 

`ifdef SIMULATION 
    localparam hcmpt = 50 ;
    localparam hcmpt2 = 16;
 `else  
    localparam hcmpt = 27'd50000000 ;
    localparam hcmpt2 = 24'd16000000;
 `endif

// LED[1] clignote a 1Hz
always @(posedge sys_clk or posedge sys_rst)

    if (sys_rst)
        begin
        LED[1] <= 0 ;
        cpt <= 0 ;
        end
    else
        if (cpt == hcmpt)
        begin
            LED[1] <= ~LED[1] ;
            cpt <= 0 ;
        end
        else 
            cpt <= cpt + 1'b1 ;

// Signal pixel_rst

logic pixel_rst ;

logic sync1 ;
logic sync2 ;

always @(posedge pixel_clk or posedge sys_rst)

    if(sys_rst)
    begin  
        sync1 <= 1 ;
        sync2 <= 1 ;
    end
    else    
    begin
        sync1 <= 0 ;
        sync2 <= sync1 ;
    end

assign pixel_rst = sync2 ; 

//LED[2] a 1Hz (avec pixel_clk)
always @(posedge pixel_clk or posedge pixel_rst)

    if (pixel_rst)
        begin
        LED[2] <= 0 ;
        cpt2 <= 0 ;
        end
    else
        if (cpt2 == hcmpt2)
        begin
            LED[2] <= ~LED[2] ;
            cpt2 <= 0 ;
        end
        else 
            cpt2 <= cpt2 + 1'b1 ;

//=============================
//  Les bus Wishbone partie 4 
//=============================
wshb_if #( .DATA_BYTES(4)) wshb_if_mire  (sys_clk, sys_rst);
wshb_if #( .DATA_BYTES(4)) wshb_if_vga (sys_clk, sys_rst);


//=============================
// Instanciation du module VGA
//=============================

vga #(.HDISP(HDISP), .VDISP(VDISP)) vga(.pixel_clk(pixel_clk), 
                                        .pixel_rst(pixel_rst), 
                                        .video_ifm(video_ifm), 
                                        .wshb_ifm(wshb_if_vga)) ;

//=============================
// Instanciation du module mire
//=============================

mire mire (.wshb_ifm(wshb_if_mire)) ;

//=============================
// Instanciation du module wshb_intercon
//=============================

wshb_intercon wshb_intercon (   .wshb_ifm(wshb_if_sdram), 
                                .wshb_ifs_mire(wshb_if_mire),
                                .wshb_ifs_vga(wshb_if_vga)
                            ) ;


endmodule
