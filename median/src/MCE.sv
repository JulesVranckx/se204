module MCE #( parameter data_size = 8 )
            ( 
            input [data_size - 1:0] A,B,
            output [data_size - 1:0] MAX, MIN
            );

	
	assign MAX = (A>=B)? A:B ;
	assign MIN = (A<B)? A:B ;

endmodule
