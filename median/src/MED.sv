module MED 	#(
		parameter data_size = 8,
		parameter nbr_data = 9
		)
		(
		input CLK,
		input [data_size - 1: 0] DI,
		input DSI,
		input BYP,
		
		output [data_size - 1: 0] DO
		);

	// Tableau de registres pour les pixels alentours
	logic [data_size - 1:0] R [nbr_data - 1:0];

	logic [data_size - 1:0] MAX,MIN ;

	wire [data_size - 1:0] MUX1, MUX2 ;

	assign MUX1 = DSI? DI:MIN ;
	assign MUX2 = BYP? R[nbr_data - 2]:MAX ;

	assign DO = R[8];

	//Gestion des sorties des multiplexeurs
	always @(posedge CLK) 
	begin 
		R[0] <= MUX1 ;
		R[nbr_data - 1] <= MUX2 ;	
		for (int i = 1; i < nbr_data - 1; i++)
			R[i] <= R[i-1] ;	
	end

	
	MCE #(.data_size(data_size)) mce (.A(R[8]), .B(R[7]), .MIN(MIN), .MAX(MAX)) ;

		
endmodule	
