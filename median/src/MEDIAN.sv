module MEDIAN 	#(
		parameter data_size = 8
		)
		(
		input CLK,
		input nRST,

		input [data_size - 1:0] DI,
		input DSI,
		
		output logic DSO,
		output [data_size - 1:0] DO
		);

	logic BYP ;
	
	logic DSI_stored ;
	logic [data_size - 1 : 0] DI_stored ; 
	
	MED #(.data_size(data_size)) med (.CLK(CLK), .DI(DI_stored), .DSI(DSI_stored), .BYP(BYP), .DO(DO)); // Instanciation du module MED

	//On génère la machine à états, qui comprend aussi des sous-états. 
	enum logic [2:0] {preload, load, E1, E2, E3, E4, E5} state;
	logic [3:0] period ;


	always @(posedge CLK or negedge nRST)
	
		if (!nRST)  
		begin
			state <= preload ;
			period <= 0 ;
			DSI_stored <= 0 ;
			DI_stored <= 0 ;
		end
	
		else 
		begin

			DSI_stored <= DSI ;
			DI_stored <= DI ;

			case (state)

				preload : 
					if (DSI) state <= load ;

				load : 
					if (!DSI) 
					begin
						state <= E1 ;
						period <= 0 ;
					end		

				E1 : 
					if (period == 8)
					begin
						state <= E2 ;
						period <= 0 ;
					end
					else 
						period <= period + 1 ;

				E2 : 
					if (period == 8)
					begin
						state <= E3 ;
						period <= 0 ;
					end
					else 
						period <= period + 1 ;
				
				E3 : 
					if (period == 8)
					begin
						state <= E4 ;
						period <= 0 ;
					end
					else 
						period <= period + 1 ;
				
				E4 : 
					if (period == 8)
					begin
						state <= E5 ;
						period <= 0 ;
					end
					else 
						period <= period + 1 ;

				E5 : 
					if (period == 4)
					begin
						state <= preload ;
					end
					else 
						period <= period + 1 ;

			endcase
		end


  // Maintenant que l'on dispose de la machine à etat, on met un bloc combinatoire donnant le comportement des signaux 

	always_comb 
	begin 

		//Valeurs par défut 
		DSO = 0 ; 
		BYP = 0 ;

		case (state) 

			load : 
				BYP = 1 ;

			E1 : 
				if (period == 8)
					BYP = 1 ;
			
			E2 : 
				if (period >= 7 )
					BYP = 1 ;
				
			E3 : 
				if (period >= 6)
					BYP = 1 ;
				
			E4 : 
				if (period >= 5)
					BYP = 1 ;

			E5 : 
				if (period == 4)
					DSO = 1 ;

		endcase
	end


endmodule