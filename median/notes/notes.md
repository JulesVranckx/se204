# Notes TP Median

## Module MCE 

### Implémentation

Pour ce module, on test simplement l'inégalité entre A et B, **sans oublier le cas d'égalité** !

### Simulation 

La simulation se déroule sans problème, avec le test bench proposé. 

### Synthèse 

Lors de la synthèse, on remarque que l'on utilise bien 24 cellules logiques, comme annoncé dans le sujet du TD. 

## Module MED 

### Implémentation

Dans ce module, on déclare un tableau qui contiendra les *nbr_data* registres :

```verilog
logic [nbr_data - 1:0][data_size - 1,0] R ;
```

On place ensuite des multiplexeurs à l'entrée des registres 0 et *nbr_data*. Enfin, on  met en place un dernier bloc séquentiel qui représente les relations entre les bascules. On instancie le module MCE avec les bons paramètres. 

### Simulation 

La simulation se déroule là encore sans problème, avec le test proposé.

### Synthèse 

Lors de la synthèse, lorsque qu'on cherche à regarder le nombre de bascules et de cellules utilisées, on peut lire ceci :

Device Utilization for EP2C35F672C

***************************************************************

Resource              Used   Avail  Utilization

\---------------------------------------------------------------

IOs                19    -       -

LUTs                25    33216   0.08%

Registers             8    33216   0.02%

Memory Bits            64    483840   0.01%

DSP block 9-bit elems       0    70     0.00%

\---------------------------------------------------------------

Le module semble utiliser 8 registres, au lieu des 9 que comporte le schéma du TD. Cependant, lorsque l'on se rend dans la partie **RTL Schematic**, on obtient le même schéma que dans le sujet. Le bloc semble donc bien avoir le comportement attendu. 

![](./notes.png)

On gardera donc ce bloc MED pour la suite du TD. 

## Module MEDIAN

### Implémentation

Pour ce dernier module, au vu de la manière dont l'énoncé est posé, on pense à faire une machine à états. On déclare pour cela : 

```verilog
enum logic [2:0] {load, E1, E2, E3, E4, E5} state;
```

Un fois que l'on dispose de la machine à états, on instancie le module MED et on s'occupe de faire avancer et de ce que fait la machine à état. 

- Dans les quatre premières étapes, on doit disposer d'un compteur allant jusque 9 qui déterminera la valeur de BYS. 

- Dans la dernière étape, il n'y a que 4 périodes.

Une fois toutes ces étapes réalisées, on peut passer DSO à 1. 

L'étape load correspond à l'étape pendant laquelle les registres sont chargés, avec donc BYP et DSI à 1. 

Une fois cette étape terminée, on passe à l'étape E1 et on peut lancer la machine à état.

J'avais dans un premier temps mis un autre état, *median*, qui était l'état pendant lequel DSO était à 1, et qui attendait que DSI repasse à 1 pour indiquer l'arrivée des pixels suivants. 



Il fallait en fait mettre des bascules pour enregistrer les valeurs de DSI et de DI, afin de les synchroniser avec la machine à état. Ces valeurs enregistrées de DSI et DI sont celles qui doivent être données au module MED. Il faut aussi un état entre la dernière étape, durant lequel la bonne valeur de DO est proposée et qui s'arrête dès que le signal DSI passe à 1.  La machine à état se présente donc comme cela :

```verilog
enum logic [2:0] {preload, load, E1, E2, E3, E4, E5} state;
logic [3:0] period ;
```

On la fait évoluer dans un bloc séquentiel qui prend en compte le possible nRST. 

Il fallait aussi créer un bloc combinatoire à part pour donner le comportement des signaux DSO et BYP. 

La phase preload correspond à une phase où le module ne fait rien, à part si le module a déjà été activé une fois, et dans ce cas DO est présenté et DSO à 1. C'est une phase d'attente. 

La phase load est la phase durant laquelle BYP et DSI sont à 1, les registres sont chargés par les valeurs présentées sur DI.

### Simulation

Lors de la première simulation, le module fonctionnait seulement pour les deux premiers tests. Pour le troisième test, il ne sortait pas la bonne valeur. Il y avait un problème dans la simulation : BYP passait à 1 pendant la première période de E2, et tout était ainsi décalé. 

Après les modifications apportées lors de l'implémentation, la simulation se déroule sans problème en utilisant le programme de test fourni. 	

### Synthèse 

On obtient ce résultat pour l'utilisation des cellules et registres :

Device Utilization for EP2C35F672C

***************************************************************

Resource              Used   Avail  Utilization

\---------------------------------------------------------------

IOs                20    -       -

LUTs                43    33216   0.13%

Registers             28    33216   0.08%

Memory Bits            64    483840   0.01%

DSP block 9-bit elems       0    70     0.00%

\---------------------------------------------------------------



On ne note pas de valeur extrême, qui correspondrait à un problème dans le code. En lisant les remarques sur l'exercice de 2008/2009, on lit qu'un valeur inquiétante pour le nombre de cellules serait de 150.

Dans le schéma donné par la simulation, on voit qu'on n'utilise que 3 registres en dehors de l'instance de MED. Ce sont les registres nécessaires pour enregistrer les valeurs de DI et DSI, ainsi que le registre contenant l'état. Pour information, voilà le schéma obtenu dans le logiciel :

![](./notes_2.png)

Dans cette première version, il reste des Warnings à la simulation, stipulant que les signaux DSI_stored et DI_stored n'ont pas de Set/Reset Assignment. On peut donc leur donner la valeur 0 dans le cas du reset. Il n'y a donc plus de Warning. 

En ce qui concerne la fréquence maximum atteinte, on lit qu'elle est de 266.241 MHz. En comparant avec les résultats des années précédentes, cette valeur semble cohérente. 



## Test sur l'image (Simulation de la synthèse)

Après synthèse, la première simulation était plus longue qu'avec le module MEDIAN.sv. 

Le fichier MEDIAN.vo contient la simulation du module MEDIAN après que celui-ci ait été synthétisé sur le modèle de FPGA Cyclone II. La simulation se déroule sur une architecture précise. 

On peut donc passer au test sur l'image :

Lors de la première simulation, on obtient une image noire. La simulation était de plus très rapide. Lorsque l'on regarde ce que contient le fichier .pgm, il ne contient que des "x". Il y a donc un problème lors de la simulation. La sortie DO du module MEDIAN ne présente que des *x*, d'où le remplissage de l'image. 

Or la simulation termine même si DO ne donne pas la bonne valeur. Quand on regarde les registres de *v*, on se rend compte qu'ils valent aussi tous x dans la simulation. 

L'erreur venait du fait que dans le fichier de test, l'image s'appelait "bogart_bruite.hex", et le fichier "bogart-bruite.hex".

En corrigeant cette erreur, on obtient bien une simulation plus longue, et la bonne image. 	