//-----------------------------------------------------------------
// Wishbone BlockRAM
//-----------------------------------------------------------------
//
// Le paramètre mem_adr_width doit permettre de déterminer le nombre 
// de mots de la mémoire : (2048 pour mem_adr_width=11)

module wb_bram #(parameter mem_adr_width = 11) (
      // Wishbone interface
      wshb_if.slave wb_s
      );

      // Inference de la memoire

      localparam n = (1 << mem_adr_width) - 1;

      logic [7:0] mem1 [0 : n] ; 
      logic [7:0] mem2 [0 : n] ;
      logic [7:0] mem3 [0 : n] ;
      logic [7:0] mem4 [0 : n] ;  
      
      // Mode Register Feedback Cycle, ne supportant que le mode BTE == 00
      
      assign wb_s.rty = 0 ;
      assign wb_s.err = 0 ;
      

      // Signal ACK
      
      logic ack_read, ack_write ;


      assign wb_s.ack = (ack_read | ack_write) ;

      assign ack_write = (wb_s.stb & wb_s.we) ;
      
      always @(posedge wb_s.clk)
            ack_read <= wb_s.stb & ~wb_s.we & (((wb_s.cti == 0 || wb_s.cti == 7) && ~ack_read) || wb_s.cti == 1 || (wb_s.cti == 2 && ~ack_read)) ;                   
      //                                          Mode classique (ou fin du burst)        |         Mode constant  |  En cours de mode incremental
      // Ecriture en memoire 

      logic [mem_adr_width - 1 : 0] addr, burst_addr ;

      assign addr = wb_s.adr[mem_adr_width + 1: 2] ; 

      // Pour definir burst_addr, soit on a cti = 001 et on est en mode constant, soit cti = 010 et on est en mode incremental, on peut donc regarder cti[1] pour decider
      // et aussi ack_read, qui repasse a 0 lors de la fin du burst. On anticipe l'adresse suivante

      assign burst_addr = addr + (ack_read & wb_s.cti[1]) ;
      
      always @(posedge wb_s.clk)
            if (wb_s.stb && wb_s.we)    
            begin
                  if (wb_s.sel[0])
                        mem4[addr] <= wb_s.dat_ms[7:0] ;  
                  if (wb_s.sel[1])
                        mem3[addr] <= wb_s.dat_ms[15:8] ;  
                  if (wb_s.sel[2]) 
                        mem2[addr] <= wb_s.dat_ms[23:16] ; 
                  if (wb_s.sel[3]) 
                        mem1[addr] <= wb_s.dat_ms[31:24] ;
            end 
	// Lecture en mémoire 
	
	always @(posedge wb_s.clk)
            wb_s.dat_sm <= {mem1[burst_addr], mem2[burst_addr], mem3[burst_addr], mem4[burst_addr]} ; 
endmodule
