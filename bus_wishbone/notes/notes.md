# TD Bus Wishbone

## Code SystemVerilog du contrôleur de mémoire

### Mise en place de la mémoire

#### Version simple

On instancie dans un premier temps une mémoire sous la forme de quatre mémoire de 2048 mots de 8 bits. On choisit cette première version pour faciliter les accès mémoire, et en particulier le fait qu'on ne peut écrire que certains octets d'un mot en mémoire. Un bloc est de la forme : 

```verilog
localparam n = (1 << mem_adr_width) - 1 ;

logic [7:0] memx [0 :n] ;
```

On accède donc à cette mémoire par une expression de la forme :

```verilog
memx[addr]
```

Cela donne un mot de 8 bits. 

On garde cette forme de mémoire pour la suite, la synthèse comprenant qu'il s'agit là d'une RAM. 

### Mode Classic Bus Cycle 

On commence par forcer les signaux *output* de l'esclave que nous n'utiliserons pas à 0. 

```verilog
assign wb_s.rty = 0 ;
assign wb_s.err = 0 ;
```

#### Pour le signal ACK 

On doit décomposer le signal **ACK** en deux signaux, un **ACK** en lecture et un **ACK** en écriture. Le **ACK** final sera un **OR** logique entre les deux. Pour le **ACK** en écriture, il suffit d'avoir les signaux **STB** et **WE** actifs (à 1).  Cela se réalise de manière combinatoire. Pour la lecture, comme notre mémoire est séquentielle, il faut un processus synchrone pour gérer ce signal. Dans un premier temps, on réalise ceci :

```verilog
always @(posedge wb_s.clk or posedge wb_s.rst)

            if (wb_s.rst)
                  ack_read <= 0 ;
            else                  
                  ack_read <= wb_s.stb & ~wb_s.we ;

```

Cette expression pour **ACK** est fausse, car on ne doit pas pouvoir avoir deux lectures à la suite. Il faut donc écrire : 

```verilog
always @(posedge wb_s.clk)

	ack_read <= wb_s.stb & ~wb_s.we & ~ack_read ;
```

On a aussi enlevé tous les resets du programme, car il n'y pas d'utilisation de ceux-ci (compteurs à remettre à 0, ...)

#### Écriture et lecture en mémoire

Pour l'écriture en mémoire, on s'intéresse aux signaux suivants : **WE**, **STB**. Il faut aussi récupérer l'adresse et la valeur à écrire. On déclare donc ce dont on a besoin. Pour l'adresse, il faut gérer le décalage, les adresse étant sur des mots de 8 bits. 

On gère l'écriture et la lecture dans le même bloc. Suivant la valeur du signal **WE**, on lit où écrit les données. 

On applique ensuite le masque grâce au signal **SEL**. 

On obtient donc un bloc combinatoire permettant de réaliser la l'écriture. 

Pour la lecture en mode classique, on se contente de donner a **DAT_SM** la valeur en memoire a l'adresse proposée de manière synchrone. 

Pour corriger l'erreur obtenue à la première simulation, on met dans **DAT_SM** la donnée contenue à l'adresse souhaitée. 

### Mode Register Feedback Cycle

Pour ce mode, on doit seulement changer la manière dont la lecture est faite. Pour cela, j'ai choisi de définir une nouvelle adresse, *burst_addr*, qui permettra d'anticiper le burst. On lit alors cette adresse en continu, et on choisi de modifier la valeur du signal **ACK**. 

## Simulation

On obtient beaucoup d'erreurs à la première simulation. On remarque en particulier que les cases mémoires ont souvent la bonne valeur, mais une lecture trop tard, comme on peut le voir ici : 

![](erreurs_simu.png)

On remarque aussi que les données sont écrites même si elles ne devaient pas l'être. C'est le bon masque qui est pris en compte, mais pas les bonnes données. 

![](./erreurs_simu2.png)

On s'aperçoit ici que c'est simplement le masque à l'envers qui est pris. Il faut donc le prendre dans l'autre sens. En corrigeant cette erreur, il reste encore une erreur sur les premières valeurs.

Le programme passe la simulation après la correction du signal **ACK**. 

## Synthèse 
 
